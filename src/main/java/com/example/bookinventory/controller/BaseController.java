package com.example.bookinventory.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookinventory.utils.Dict;

@RestController
@RequestMapping("")
public class BaseController {
    /**
     * Handles the root endpoint and returns a greeting message.
     *
     * @return ResponseEntity containing the greeting message and HTTP status OK.
     */
    @GetMapping
    public ResponseEntity<Dict> greeting() {
        Dict response = new Dict();
        response.put("message", "Hello world");
        return ResponseEntity.ok(response);
    }
}
