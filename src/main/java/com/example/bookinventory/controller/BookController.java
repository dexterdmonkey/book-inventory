package com.example.bookinventory.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Page;

import com.example.bookinventory.errors.ErrorBodyGenerator;
import com.example.bookinventory.errors.RestException;
import com.example.bookinventory.model.Book;
import com.example.bookinventory.service.BookService;
import com.example.bookinventory.utils.Dict;
import com.example.bookinventory.utils.Pagination;

@RestController
@RequestMapping("/book")
public class BookController {
    private static final Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    BookService bookService;

    @PostMapping("")
    public ResponseEntity<Book> createBook(@RequestBody Book request) {
        Book book = bookService.createBook(request);
        return new ResponseEntity<>(book, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Book> getBook(@PathVariable String id) {
        Book book = bookService.getBook(id);
        if (book == null) {
            Dict errorResponse = ErrorBodyGenerator.createNotFoundResponse("book", id);
            throw new RestException(errorResponse);
        }
        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Book> updateBook(@PathVariable String id, @RequestBody Book request) {
        Book book = bookService.updateBook(id, request);
        if (book == null) {
            Dict errorResponse = ErrorBodyGenerator.createNotFoundResponse("book", id);
            throw new RestException(errorResponse);
        }
        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Dict> deleteBook(@PathVariable String id) {
        Boolean found = bookService.deleteBook(id);
        if (!found) {
            Dict errorResponse = ErrorBodyGenerator.createNotFoundResponse("book", id);
            throw new RestException(errorResponse);
        }

        Dict response = new Dict();
        response.put("success", true);
        return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping("")
    public ResponseEntity<Dict> getBooks(
            @RequestParam Map<String, String> queryParams,
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int rowLength,
            @RequestParam(required = false, defaultValue = "") String sortBy,
            @RequestParam(required = false, defaultValue = "") String publisher) {

        Map<String, Boolean> parsedSortBy = Pagination.parseSortBy(sortBy, "createdAt.desc");
        Map<String, String> filters = Pagination.parseFilters(queryParams);
        Page<Book> bookPage;

        if (publisher != "" && !publisher.isEmpty()) {
            bookPage = bookService.getBooks(publisher);
        } else {
            bookPage = bookService.getBooks(pageNumber, rowLength, filters,
                    parsedSortBy);
        }
        List<Book> books = bookPage.getContent();
        Dict response = new Dict();
        response.put("books", books);
        response.put("pageNumber", bookPage.getNumber());
        response.put("totalItems", bookPage.getTotalElements());
        response.put("totalPages", bookPage.getTotalPages());
        logger.info("response: " + response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
