package com.example.bookinventory.errors;

import org.springframework.http.HttpStatus;
import lombok.Getter;

import com.example.bookinventory.utils.Dict;

/**
 * Custom exception class for REST API error handling.
 * This exception class allows for detailed error responses including HTTP
 * status codes,
 * error messages, and debug messages.
 */
@Getter
public class RestException extends RuntimeException {
    private String debugMessage;
    private HttpStatus httpStatus;
    private Dict errorBody;

    /**
     * Constructs a new RestException with the specified error body.
     * 
     * @param errorBody the detailed error information as a Dict object
     */
    public RestException(Dict errorBody) {
        super(errorBody.getDict("error").getString("message"));
        this.errorBody = errorBody;
        this.httpStatus = HttpStatus.resolve(this.errorBody.getInt("statusCode"));
    }

    /**
     * Constructs a new RestException with the specified error body and message.
     * 
     * @param errorBody the detailed error information as a Dict object
     * @param message   the error message
     */
    public RestException(Dict errorBody, String message) {
        super(message);
        this.errorBody = errorBody;
        this.httpStatus = HttpStatus.resolve(this.errorBody.getInt("statusCode"));
    }

    /**
     * Constructs a new RestException with the specified error body and cause.
     * 
     * @param errorBody the detailed error information as a Dict object
     * @param cause     the cause of the exception
     */
    public RestException(Dict errorBody, Throwable cause) {
        super(cause.getLocalizedMessage());
        this.errorBody = errorBody;
        this.httpStatus = HttpStatus.resolve(this.errorBody.getInt("statusCode"));
    }

    /**
     * Constructs a new RestException with the specified error code.
     * 
     * @param code the error code
     */
    public RestException(String code) {
        super(ErrorBodyGenerator.getErrorMessage(code));
        this.errorBody = ErrorBodyGenerator.createErrorResponse(code);
        this.httpStatus = HttpStatus.resolve(this.errorBody.getInt("statusCode"));
    }

    /**
     * Constructs a new RestException with the specified error code and message.
     * 
     * @param code    the error code
     * @param message the error message
     */
    public RestException(String code, String message) {
        super(message);
        this.errorBody = ErrorBodyGenerator.createErrorResponse(code, message);
        this.httpStatus = HttpStatus.resolve(this.errorBody.getInt("statusCode"));
    }

    /**
     * Constructs a new RestException with the specified error code and cause.
     * 
     * @param code  the error code
     * @param cause the cause of the exception
     */
    public RestException(String code, Throwable cause) {
        super(ErrorBodyGenerator.getErrorMessage(code));
        this.debugMessage = cause.getLocalizedMessage();
        this.errorBody = ErrorBodyGenerator.createErrorResponse(code);
        this.httpStatus = HttpStatus.resolve(this.errorBody.getInt("statusCode"));
    }

    /**
     * Constructs a new RestException with the specified error code, message, and
     * cause.
     * 
     * @param code    the error code
     * @param message the error message
     * @param cause   the cause of the exception
     */
    public RestException(String code, String message, Throwable cause) {
        super(message);
        this.debugMessage = cause.getLocalizedMessage();
        this.errorBody = ErrorBodyGenerator.createErrorResponse(code, message);
        this.httpStatus = HttpStatus.resolve(this.errorBody.getInt("statusCode"));
    }
}
