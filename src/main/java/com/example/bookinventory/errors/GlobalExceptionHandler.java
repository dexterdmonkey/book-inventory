package com.example.bookinventory.errors;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import com.example.bookinventory.utils.Dict;

/**
 * Global exception handler for handling various types of exceptions in the
 * application. It provides centralized error handling and response generation
 * for common exceptions. This class uses Spring's `@ControllerAdvice`
 * annotation to define global exception handling for all controllers in the
 * application.
 *
 * It handles exceptions such as `NoResourceFoundException` for 404 Not Found
 * errors, `IllegalArgumentException` for 400 Bad Request errors, and other
 * specific exceptions.
 * 
 * References:
 * - https://www.toptal.com/java/spring-boot-rest-api-error-handling
 * 
 * For each exception, it logs the error and generates an appropriate error
 * response using the `ErrorResponse` class.
 * 
 * @see NoResourceFoundException
 * @see IllegalArgumentException
 * @see ConstraintViolationException
 * @see MethodArgumentTypeMismatchException
 * @see HttpRequestMethodNotSupportedException
 * @see HttpMessageNotReadableException
 * @see DataIntegrityViolationException
 * @see Exception
 * @see ErrorBodyGenerator
 */
@Order(Ordered.LOWEST_PRECEDENCE)
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * Handles general exceptions thrown by the application. Logs the error and
     * generates an internal server error response.
     *
     * @param e The exception that was thrown
     * @return A ResponseEntity containing an error response
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Dict> handleException(Exception e) {
        Dict errorResponse = ErrorBodyGenerator.createErrorResponse();
        logger.error("An error occurred: ", e);
        logger.error("errorResponse: " + errorResponse);
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(errorResponse);
    }

    /**
     * Handles NoResourceFoundException, typically thrown when a resource is not
     * found. Generates a 404 Not Found error response.
     *
     * @param e The exception that was thrown
     * @return A ResponseEntity containing an error response
     */
    @ExceptionHandler(NoResourceFoundException.class)
    protected ResponseEntity<Object> handleNoResourceFound(
            NoResourceFoundException e) {
        Dict errorResponse = ErrorBodyGenerator.createErrorResponse("404440");
        logger.error("errorResponse: " + errorResponse);
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(errorResponse);
    }

    /**
     * Handles IllegalArgumentException, typically thrown when an illegal argument
     * is passed. Generates a 400 Bad Request error response.
     *
     * @param e The exception that was thrown
     * @return A ResponseEntity containing an error response
     */
    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<Object> handleIllegalArgumentException(
            IllegalArgumentException e) {
        Dict errorResponse = ErrorBodyGenerator.createErrorResponse("400095", e.getLocalizedMessage());
        logger.error("errorResponse: " + errorResponse);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(errorResponse);
    }

    /**
     * Handles ConstraintViolationException, typically thrown when a constraint is
     * violated. Generates a 400 Bad Request error response.
     *
     * @param e The exception that was thrown
     * @return A ResponseEntity containing an error response
     */
    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(
            ConstraintViolationException e) {
        Dict errorResponse = ErrorBodyGenerator.createErrorResponse("400096", e.getLocalizedMessage());
        logger.error("errorResponse: " + errorResponse);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(errorResponse);
    }

    /**
     * Handles MethodArgumentTypeMismatchException, typically thrown when a method
     * argument fails to match the expected type. Generates a 400 Bad Request error
     * response.
     *
     * @param e The exception that was thrown
     * @return A ResponseEntity containing an error response
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException e) {
        Dict errorResponse = ErrorBodyGenerator.createErrorResponse("400097", e.getLocalizedMessage());
        logger.error("errorResponse: " + errorResponse);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(errorResponse);
    }

    /**
     * Handles HttpRequestMethodNotSupportedException, typically thrown when an
     * unsupported HTTP method is used. Generates a 400 Bad Request error response.
     *
     * @param e The exception that was thrown
     * @return A ResponseEntity containing an error response
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException e) {
        Dict errorResponse = ErrorBodyGenerator.createErrorResponse("400098", e.getMessage());
        logger.error("errorResponse: " + errorResponse);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(errorResponse);
    }

    /**
     * Handles HttpMessageNotReadableException, typically thrown when there is a
     * problem with the request message. Generates a 400 Bad Request error response.
     *
     * @param e The exception that was thrown
     * @return A ResponseEntity containing an error response
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            HttpMessageNotReadableException e) {
        Dict errorResponse = ErrorBodyGenerator.createErrorResponse("400099");
        logger.error("errorResponse: " + errorResponse);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(errorResponse);
    }

}