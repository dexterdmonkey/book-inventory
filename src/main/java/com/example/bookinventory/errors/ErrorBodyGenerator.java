package com.example.bookinventory.errors;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import com.example.bookinventory.utils.Dict;
import com.example.bookinventory.utils.Helpers;

/**
 * A utility class for generating error responses with error messages and HTTP
 * status codes.
 */
public class ErrorBodyGenerator {
    private static final Map<String, String> errorList = new HashMap<>();
    static {
        errorList.put("400999", "Bad request");
        errorList.put("400099", "Malformed JSON request");
        errorList.put("401001", "Unauthorized");
        errorList.put("401002", "Session expired");
        errorList.put("403013", "Permission denied");
        errorList.put("404399", "Not found");
        errorList.put("404440", "No resource found");
        errorList.put("500000", "Internal server error");
    }

    /**
     * Returns the error message associated with the specified error code.
     *
     * @param code the error code.
     * @return the error message corresponding to the error code.
     */
    public static String getErrorMessage(String code) {
        return errorList.get(code);
    }

    /**
     * Parses the HTTP status code from the given error code.
     *
     * @param code The error code.
     * @return The HTTP status code parsed from the error code.
     */
    private static int parseHttpStatusCode(String code) {
        if (code == null || code.length() < 3) {
            return 0;
        }

        String firstThreeDigitsString = code.substring(0, 3);
        if (!firstThreeDigitsString.matches("\\d+")) {
            return 0;
        }
        return Integer.parseInt(firstThreeDigitsString);
    }

    /**
     * Creates an error response with the given error code and message.
     *
     * @param errorCode    The error code.
     * @param errorMessage The error message.
     * @return The error response as a dictionary.
     */
    public static Dict createErrorResponse(String errorCode, String errorMessage) {
        Dict error = new Dict();
        error.put("message", errorMessage);
        error.put("code", errorCode);

        Dict response = new Dict();
        response.put("timestamp", LocalDateTime.now());
        response.put("traceId", Helpers.createID());
        response.put("error", error);
        response.put("statusCode", parseHttpStatusCode(errorCode));
        return response;
    }

    /**
     * Creates an error response with the given error code.
     *
     * @param errorCode The error code.
     * @return The error response as a dictionary.
     */
    public static Dict createErrorResponse(String errorCode) {
        String errorMessage = errorList.get(errorCode);
        return createErrorResponse(errorCode, errorMessage);
    }

    /**
     * Creates a default error response with the error code "500000" (Internal
     * Server Error).
     *
     * @return The error response as a dictionary.
     */
    public static Dict createErrorResponse() {
        String errorCode = "500000";
        String errorMessage = errorList.get(errorCode);
        return createErrorResponse(errorCode, errorMessage);
    }

    // ...continue for custom error code
    /**
     * Creates a "not found" error response with the error code "404440".
     *
     * @return The error response as a dictionary.
     */
    public static Dict createNotFoundResponse() {
        String errorCode = "404440";
        String errorMessage = errorList.get(errorCode);
        return createErrorResponse(errorCode, errorMessage);
    }

    /**
     * Creates a "not found" error response with a custom entity name and ID.
     *
     * @param entityName The name of the entity.
     * @param entityId   The ID of the entity.
     * @return The error response as a dictionary.
     */
    public static Dict createNotFoundResponse(String entityName, String entityId) {
        String errorCode = "404441";
        String errorMessage = String.format("Not found %s with id %s", entityName, entityId);
        return createErrorResponse(errorCode, errorMessage);
    }
}
