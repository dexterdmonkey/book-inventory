package com.example.bookinventory.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.example.bookinventory.utils.Dict;

/**
 * Global exception handler for handling custom RestExceptions in the
 * application.
 *
 * This class uses the @ControllerAdvice annotation to handle exceptions
 * globally across the whole application. It catches RestExceptions and provides
 * a structured response with error details and HTTP status codes.
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    /**
     * Handles RestException and constructs a ResponseEntity containing the error
     * details.
     *
     * @param e the RestException to handle
     * @return a ResponseEntity containing the error response with appropriate HTTP
     *         status code
     */
    @ExceptionHandler(RestException.class)
    public ResponseEntity<Dict> handleException(RestException e) {
        Dict errorResponse = e.getErrorBody();
        logger.error("An error occurred: " + e);
        logger.error("errorResponse: " + errorResponse);
        return ResponseEntity
                .status(e.getHttpStatus())
                .body(errorResponse);
    }

}