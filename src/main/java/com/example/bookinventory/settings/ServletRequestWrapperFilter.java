package com.example.bookinventory.settings;

import java.io.IOException;

import org.springframework.stereotype.Component;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;

/**
 * A filter that removes trailing slashes from the request URI to standardize
 * the URL format. This filter wraps the original HttpServletRequest and
 * modifies the request URI if it ends with a trailing slash.
 * 
 * This ensures consistent URL handling in the application, making both
 * /users and /users/ valid and effectively the same endpoint.
 * 
 * Example:
 * - /users (valid)
 * - /users/ (valid, but will be normalized to /users)
 * 
 * References:
 * - https://github.com/spring-projects/spring-framework/issues/28552
 * - https://codaholic.com/trailing-slash-filter/
 */
@Component
public class ServletRequestWrapperFilter implements Filter {

    /**
     * Filters the incoming requests and removes trailing slashes from the request
     * URI.
     *
     * @param request  the ServletRequest object that contains the request the
     *                 client made to the servlet
     * @param response the ServletResponse object that contains the response the
     *                 servlet returns to the client
     * @param chain    the FilterChain for invoking the next filter or the resource
     * @throws IOException      if an I/O error occurs during this filter's
     *                          processing
     * @throws ServletException if the processing fails for some reason
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String path = httpRequest.getRequestURI();
        if (path.endsWith("/")) {
            String newPath = path.substring(0, path.length() - 1);
            HttpServletRequest newRequest = new HttpRequestWrapper(httpRequest, newPath);
            chain.doFilter(newRequest, response);
        } else {
            chain.doFilter(request, response);
        }
    }

    /**
     * A wrapper class for HttpServletRequest that allows modification of the
     * request URI.
     */
    private static class HttpRequestWrapper extends HttpServletRequestWrapper {
        private final String newPath;

        /**
         * Constructs a new HttpRequestWrapper.
         *
         * @param request the original HttpServletRequest
         * @param newPath the modified request URI without the trailing slash
         */
        public HttpRequestWrapper(HttpServletRequest request, String newPath) {
            super(request);
            this.newPath = newPath;
        }

        /**
         * Returns the modified request URI.
         *
         * @return the modified request URI
         */
        @Override
        public String getRequestURI() {
            return newPath;
        }

        /**
         * Returns the modified request URL.
         *
         * @return the modified request URL
         */
        @Override
        public StringBuffer getRequestURL() {
            StringBuffer url = new StringBuffer();
            url.append(getScheme())
                    .append("://")
                    .append(getServerName())
                    .append(":")
                    .append(getServerPort())
                    .append(newPath);
            return url;
        }
    }
}
