package com.example.bookinventory.model;

import java.time.LocalDateTime;
import java.time.LocalDate;
import java.util.Arrays;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.type.SqlTypes;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import com.example.bookinventory.utils.Dict;
import com.example.bookinventory.utils.Helpers;

/**
 * Represents an book entity with properties such as name, status, etc.
 * Provides methods for generating a unique ID and converting the object to a
 * JSON string.
 */
@Getter
@Setter
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "m_books", indexes = {
        @Index(name = "idx_m_books_name", columnList = "name"),
        @Index(name = "idx_m_books_email", columnList = "email"),
        @Index(name = "idx_m_books_created_at", columnList = "created_at")
})
public class Book {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "title", columnDefinition = "VARCHAR(255) DEFAULT '' NOT NULL")
    private String title = "";

    @Column(name = "publisher", columnDefinition = "VARCHAR(255) DEFAULT '' NOT NULL")
    private String publisher = "";

    @Column(name = "author", columnDefinition = "VARCHAR(255) DEFAULT '' NOT NULL")
    private String author = "";

    @Column(name = "status", columnDefinition = "VARCHAR(255) DEFAULT '' NOT NULL")
    private String status = "";

    @Column(name = "issuedAt", columnDefinition = "TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL")
    private LocalDate issuedAt;

    @Column(name = "createdAt", columnDefinition = "TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL")
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = "updatedAt", columnDefinition = "TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL")
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "data", columnDefinition = "JSONB DEFAULT '{}' NOT NULL")
    private Dict data = new Dict();

    /**
     * Checks if a given filter key is in the list of supported filters.
     *
     * @param filterKey the filter key to check
     * @return true if the filter key is supported, false otherwise
     */
    public static boolean isSupportedFilter(String filterKey) {
        String[] supportedFilters = { "title", "publisher", "author", "status" };
        return Arrays.asList(supportedFilters).contains(filterKey);
    }

    /**
     * Checks if a given sort key is in the list of supported sorts.
     *
     * @param sortKey the sort key to check
     * @return true if the sort key is supported, false otherwise
     */
    public static boolean isSupportedSort(String sortKey) {
        String[] supportedSorts = { "title", "issuedAt" };
        return Arrays.asList(supportedSorts).contains(sortKey);
    }

    /**
     * Generates a unique ID for the book.
     */
    public void generateId() {
        this.id = Helpers.createID();
    }

    /**
     * Converts the book object to a JSON string.
     *
     * @return the JSON string representation of the book
     */
    public String toString() {
        return Helpers.toJsons(this);
    }
}
