package com.example.bookinventory.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Utility class for handling pagination and sorting parameters.
 */
public class Pagination {

    /**
     * Parses the sortBy parameter and returns a map of sorting fields with their
     * corresponding sort order.
     * If the sortBy parameter is empty, it defaults to the provided defaultSort.
     *
     * @param sortByParam the sorting parameters as a comma-separated string (e.g.,
     *                    "name.asc,age.desc").
     * @param defaultSort the default sorting parameters to use if sortByParam is
     *                    empty.
     * @return a map where the key is the field name and the value is true for
     *         ascending and false for descending.
     */
    public static Map<String, Boolean> parseSortBy(String sortByParam, String defaultSort) {
        Map<String, Boolean> sortByMap = new HashMap<>();
        sortByMap = parseSortBy(sortByParam);
        if (sortByMap.isEmpty()) {
            sortByMap = parseSortBy(defaultSort);
        }
        return sortByMap;
    }

    /**
     * Parses the sortBy parameter and returns a map of sorting fields with their
     * corresponding sort order.
     *
     * @param sortByParam the sorting parameters as a comma-separated string (e.g.,
     *                    "name.asc,age.desc").
     * @return a map where the key is the field name and the value is true for
     *         ascending and false for descending.
     */
    public static Map<String, Boolean> parseSortBy(String sortByParam) {
        Map<String, Boolean> sortByMap = new HashMap<>();
        String[] parts = sortByParam.split(",");
        for (String part : parts) {
            String[] subparts = part.split("\\.");
            if (subparts.length == 2) {
                String key = subparts[0];
                boolean asc = !"desc".equalsIgnoreCase(subparts[1]);
                sortByMap.put(key, asc);
            }
        }
        return sortByMap;
    }

    /**
     * Parses filter parameters from the query parameters and returns a map of
     * filters.
     *
     * @param queryParams a map of query parameters where filter keys are in the
     *                    format "filter[key]".
     * @return a map where the key is the filter name and the value is the filter
     *         value.
     */
    public static Map<String, String> parseFilters(Map<String, String> queryParams) {
        Map<String, String> filters = new HashMap<>();
        for (Map.Entry<String, String> filter : queryParams.entrySet()) {
            if (!filter.getKey().startsWith("filter[") || !filter.getKey().endsWith("]")) {
                continue; // Skip filters that are not formatted as filter[key]
            }
            String key = filter.getKey()
                    .replace("filter[", "")
                    .replace("]", "");
            String value = filter.getValue();
            filters.put(key, value);
        }
        return filters;
    }

}
