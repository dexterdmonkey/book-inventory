package com.example.bookinventory.utils;

import java.security.SecureRandom;
import java.time.Instant;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * A collection of utility methods for generating IDs, OTPs, random strings,
 * and other helpful functions.
 */
public class Helpers {
    private static final SecureRandom random = new SecureRandom();
    private static final ObjectMapper objectMapper = new ObjectMapper();
    static {
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    /**
     * Creates a unique ID using a timestamp and a random UUID.
     *
     * @param timestamp the timestamp to include in the ID, or null to use the
     *                  current timestamp
     * @return a unique ID string
     */
    public static String createID(Long timestamp) {
        timestamp = timestamp != null ? timestamp : Instant.now().getEpochSecond();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        return Long.toHexString(timestamp) + uuid;
    }

    /**
     * Creates a unique ID using the current timestamp and a random UUID.
     *
     * @return a unique ID string
     */
    public static String createID() {
        long timestamp = Instant.now().getEpochSecond();
        return Helpers.createID(timestamp);
    }

    /**
     * Creates a short ID using the current timestamp.
     *
     * @return a short ID string
     */
    public static String createShortID() {
        return Long.toString(Instant.now().toEpochMilli(), 36);
    }

    /**
     * Creates a random OTP (One-Time Password) of the specified length.
     *
     * @param length the length of the OTP
     * @return a random OTP string
     */
    public static String createOTP(int length) {
        String charset = "0123456789";
        StringBuilder otp = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(charset.length());
            otp.append(charset.charAt(randomIndex));
        }
        return otp.toString();
    }

    /**
     * Generates a random string of the specified length using the given charset.
     *
     * @param length the length of the random string
     * @return a random string
     */
    public static String createRandomString(int length) {
        String charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(charset.length());
            result.append(charset.charAt(randomIndex));
        }
        return result.toString();
    }

    /**
     * Returns a random element from the given array.
     *
     * @param values the array of values
     * @param <T>    the type of the values in the array
     * @return a random value from the array
     */
    public static <T> T getRandomFrom(T[] values) {
        int randomIndex = random.nextInt(values.length);
        return values[randomIndex];
    }

    /**
     * Converts a UNIX timestamp to a date string.
     *
     * @param t the UNIX timestamp
     * @return the date string
     */
    public static String castUnixAsDatestring(long t) {
        Instant instant = Instant.ofEpochSecond(t);
        return instant.toString();
    }

    /**
     * Inverts the keys and values of a map.
     *
     * @param inputMap the input map
     * @param <K>      the type of the keys in the input map
     * @param <V>      the type of the values in the input map
     * @return the inverted map
     */
    public static <K, V> java.util.Map<V, K> invertMap(java.util.Map<K, V> inputMap) {
        java.util.Map<V, K> invertedMap = new java.util.HashMap<>();
        for (java.util.Map.Entry<K, V> entry : inputMap.entrySet()) {
            invertedMap.put(entry.getValue(), entry.getKey());
        }
        return invertedMap;
    }

    /**
     * Converts a boolean value to an integer (0 for false, 1 for true).
     *
     * @param b the boolean value
     * @return the integer value
     */
    public static int btoi(boolean b) {
        return b ? 1 : 0;
    }

    /**
     * Converts a string to an integer, or returns 0 if the string cannot be parsed.
     *
     * @param s the string
     * @return the integer value
     */
    public static int stoi(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * Converts an object to its JSON representation.
     *
     * @param o the object
     * @return the JSON string representation of the object
     */
    public static String toJsons(Object o) {
        try {
            return objectMapper.writeValueAsString(o);
        } catch (Exception e) {
            System.out.println(String.format("ERROR: %s", e));
            return "{}";
        }
    }
}
