package com.example.bookinventory.utils;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * A utility class that extends HashMap and provides JSON serialization and
 * deserialization capabilities.
 */
public class Dict extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;
    private static final ObjectMapper objectMapper = new ObjectMapper();
    static {
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    /**
     * Default constructor.
     */
    public Dict() {
        super();
    }

    /**
     * Constructs a Dict from a JSON string.
     *
     * @param json JSON string representation of the Dict.
     */
    public Dict(String json) {
        super();
        fromJson(json);
    }

    /**
     * Populates the Dict with data from a JSON string.
     *
     * @param json JSON string representation of the Dict.
     */
    public void fromJson(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Dict dict = objectMapper.readValue(json, Dict.class);
            this.putAll(dict);
        } catch (Exception e) {
            // Handle exception
        }
    }

    /**
     * Converts the Dict to a JSON string.
     *
     * @return JSON string representation of the Dict.
     */
    public String toJson() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (Exception e) {
            return "{}";
        }
    }

    /**
     * Converts the Dict to a JSON string.
     *
     * @return JSON string representation of the Dict.
     */
    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (Exception e) {
            return super.toString();
        }
    }

    /**
     * Gets the value associated with the specified key as a String, or a default
     * value if the key is not found or the value is not a String.
     *
     * @param key          the key whose associated value is to be returned
     * @param defaultValue the default value to return if the key is not found or
     *                     the value is not a String
     * @return the value associated with the specified key as a String, or the
     *         default value if no value is present or the value is not a String
     */
    public String getString(String key, String defaultValue) {
        Object value = this.get(key);
        return value instanceof String ? (String) value : defaultValue;
    }

    /**
     * Gets the value associated with the specified key as a String, or null if the
     * key is not found.
     *
     * @param key the key whose associated value is to be returned
     * @return the value associated with the specified key as a String, or null if
     *         no value is present
     */
    public String getString(String key) {
        return getString(key, null);
    }

    /**
     * Gets the value associated with the specified key as an Integer, or a default
     * value if the key is not found or the value is not an Integer.
     *
     * @param key          the key whose associated value is to be returned
     * @param defaultValue the default value to return if the key is not found or
     *                     the value is not an Integer
     * @return the value associated with the specified key as an Integer, or the
     *         default value if no value is present or the value is not an Integer
     */
    public Integer getInt(String key, Integer defaultValue) {
        Object value = this.get(key);
        return value instanceof Integer ? (Integer) value : defaultValue;
    }

    /**
     * Gets the value associated with the specified key as an Integer, or null if
     * the key is not found.
     *
     * @param key the key whose associated value is to be returned
     * @return the value associated with the specified key as an Integer, or null if
     *         no value is present
     */
    public Integer getInt(String key) {
        return getInt(key, null);
    }

    /**
     * Gets the value associated with the specified key as a Dict, or a default
     * value if the key is not found or the value is not a Dict.
     *
     * @param key          the key whose associated value is to be returned
     * @param defaultValue the default value to return if the key is not found or
     *                     the value is not a Dict
     * @return the value associated with the specified key as a Dict, or the default
     *         value if no value is present or the value is not a Dict
     */
    public Dict getDict(String key, Dict defaultValue) {
        Object value = this.get(key);
        return value instanceof Dict ? (Dict) value : defaultValue;
    }

    /**
     * Gets the value associated with the specified key as a Dict, or null if the
     * key is not found.
     *
     * @param key the key whose associated value is to be returned
     * @return the value associated with the specified key as a Dict, or null if no
     *         value is present
     */
    public Dict getDict(String key) {
        return getDict(key, null);
    }

    /**
     * Gets the value associated with the specified key as a Boolean, or a default
     * value if the key is not found or the value is not a Boolean.
     *
     * @param key          the key whose associated value is to be returned
     * @param defaultValue the default value to return if the key is not found or
     *                     the value is not a Boolean
     * @return the value associated with the specified key as a Boolean, or the
     *         default value if no value is present or the value is not a Boolean
     */
    public Boolean getBool(String key, Boolean defaultValue) {
        Object value = this.get(key);
        return value instanceof Boolean ? (Boolean) value : defaultValue;
    }

    /**
     * Gets the value associated with the specified key as a Boolean, or null if the
     * key is not found.
     *
     * @param key the key whose associated value is to be returned
     * @return the value associated with the specified key as a Boolean, or null if
     *         no value is present
     */
    public Boolean getBool(String key) {
        return getBool(key, null);
    }

    /**
     * Gets the value associated with the specified key as a List of type T, or a
     * default value if the key is not found or the value is not a List.
     *
     * @param key          the key whose associated value is to be returned
     * @param defaultValue the default value to return if the key is not found or
     *                     the value is not a List
     * @return the value associated with the specified key as a List of type T, or
     *         the default value if no value is present or the value is not a List
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> getList(String key, List<T> defaultValue) {
        Object value = this.get(key);
        return value instanceof List ? (List<T>) value : defaultValue;
    }

    /**
     * Gets the value associated with the specified key as a List.
     * 
     * @param key the key whose associated value is to be returned
     * @return the value associated with the specified key as a List, or
     *         {@code null} if no value is present
     */
    public <T> List<T> getList(String key) {
        return getList(key, null);
    }

    /**
     * Gets the value associated with the specified key as a Double, or a default
     * value if the key is not found or the value is not a Double.
     *
     * @param key          the key whose associated value is to be returned
     * @param defaultValue the default value to return if the key is not found or
     *                     the value is not a Double
     * @return the value associated with the specified key as a Double, or the
     *         default value if no value is present or the value is not a Double
     */
    public Double getDouble(String key, Double defaultValue) {
        Object value = this.get(key);
        return value instanceof Double ? (Double) value : defaultValue;
    }

    /**
     * Gets the value associated with the specified key as a Double.
     * 
     * @param key the key whose associated value is to be returned
     * @return the value associated with the specified key as a Double, or
     *         {@code null} if no value is present
     */
    public Double getDouble(String key) {
        return getDouble(key, null);
    }
}
