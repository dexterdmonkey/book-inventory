package com.example.bookinventory.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.bookinventory.model.Book;

public interface BookRepository extends JpaRepository<Book, String> {
    Page<Book> findByPublisher(String publisher, Pageable pageable);
    Page<Book> findByTitleContaining(String title, Pageable pageable);
}
