package com.example.bookinventory.service;

import java.util.Optional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import com.example.bookinventory.model.Book;
import com.example.bookinventory.repository.BookRepository;

@Service
@Transactional
public class BookService {
    private static final Logger logger = LoggerFactory.getLogger(BookService.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    BookRepository bookRepository;

    public Book createBook(Book book) {
        book.generateId();
        logger.info(String.format("begin to create book, id: %s", book.getId()));
        Book save = bookRepository.save(book);
        logger.info(String.format("end to create book"));
        return save;
    }

    public Book getBook(String id) {
        logger.info(String.format("begin to get book, id: %s", id));
        Optional<Book> book = bookRepository.findById(id);
        logger.info(String.format("end to get book"));
        if (book.isEmpty()) {
            return null;
        }
        return book.get();
    }

    public Book updateBook(String id, Book book) {
        final Book result = getBook(id);
        if (result == null) {
            return null;
        }

        logger.info(String.format("begin to update book, id: %s", id));
        bookRepository.save(result);
        logger.info(String.format("end to update book"));
        return result;
    }

    public Boolean deleteBook(String id) {
        final Book book = getBook(id);
        if (book == null) {
            return false;
        }

        logger.info(String.format("begin to delete book, id: %s", id));
        bookRepository.deleteById(id);
        logger.info(String.format("end to delete book"));
        return true;
    }

    public Page<Book> getBooks(int page, int size, Map<String, String> filters,
            Map<String, Boolean> sortBy) {
        Pageable paging = PageRequest.of(page, size);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        // Main query for fetching results
        CriteriaQuery<Book> query = cb.createQuery(Book.class);
        Root<Book> root = query.from(Book.class);
        List<Predicate> predicates = new ArrayList<>();

        for (Map.Entry<String, String> filter : filters.entrySet()) {
            String key = filter.getKey();
            String value = filter.getValue();
            if (!Book.isSupportedFilter(key)) {
                continue;
            }

            logger.info(String.format("filter by %s '%s'", key, value));
            if (value.startsWith("eq:")) {
                String eqValue = value.substring(3); // Remove "eq:" prefix
                predicates.add(cb.equal(cb.lower(root.get(key)), eqValue.toLowerCase()));
            } else if (value.startsWith("like:")) {
                String likeValue = value.substring(5); // Remove "like:" prefix
                predicates.add(cb.like(cb.lower(root.get(key)), likeValue.toLowerCase() + "%"));
            } else if (value.startsWith("neq:")) {
                String neqValue = value.substring(4); // Remove "neq:" prefix
                predicates.add(cb.notEqual(cb.lower(root.get(key)), neqValue.toLowerCase()));
            }
        }

        query.where(predicates.toArray(new Predicate[0]));

        // Sorting
        List<Order> orders = new ArrayList<>();
        for (Map.Entry<String, Boolean> entry : sortBy.entrySet()) {
            String key = entry.getKey();
            Boolean asc = entry.getValue();
            if (!Book.isSupportedSort(key)) {
                continue;
            }

            logger.info(String.format("sort by %s [asc:%s]", key, asc));
            if (asc) {
                orders.add(cb.asc(root.get(key)));
            } else {
                orders.add(cb.desc(root.get(key)));
            }
        }
        query.orderBy(orders);

        // Fetch results
        logger.info(String.format("begin to get books, page: %d, size: %d", page, size));
        List<Book> books = entityManager.createQuery(query)
                .setFirstResult((int) paging.getOffset())
                .setMaxResults(paging.getPageSize())
                .getResultList();
        logger.info("end to get books");

        // Count query
        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<Book> countRoot = countQuery.from(Book.class);
        countQuery.select(cb.count(countRoot));

        logger.info("begin to count books");
        long count = entityManager.createQuery(countQuery).getSingleResult();
        logger.info("end to count books");

        return new PageImpl<>(books, paging, count);
    }

    public Page<Book> getBooks(String publisher) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> query = cb.createQuery(Book.class);
        Root<Book> root = query.from(Book.class);

        List<Predicate> predicates = new ArrayList<>();
        if (publisher != null && !publisher.isEmpty()) {
            logger.info(String.format("filter by like publisher: %s", publisher));
            predicates.add(cb.like(cb.lower(root.get("publisher")), publisher.toLowerCase() + "%"));
        }

        query.where(predicates.toArray(new Predicate[0]));
        logger.info(String.format("begin to get books"));
        List<Book> books = entityManager.createQuery(query).getResultList();
        logger.info(String.format("end to get books"));

        int count = books.size();
        Pageable paging = PageRequest.of(0, count);
        return new PageImpl<>(books, paging, count);
    }
}
