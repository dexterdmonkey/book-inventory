package com.example.bookinventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

/**
 * Main application class for the book inventory project.
 * This class serves as the entry point for the Spring Boot application.
 * It excludes the default Spring Security auto-configuration.
 * 
 * Author: dextermonkey
 * Build Date: June 2024
 * Usage:
 * - Run this class to start the Spring Boot application.
 * 
 * License:
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Example:
 * {@code
 * public static void main(String[] args) {
 *     SpringApplication.run(Application.class, args);
 * }
 * }
 */
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class BookinventoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookinventoryApplication.class, args);
    }

}
